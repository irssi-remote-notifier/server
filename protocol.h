
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#ifndef DEF_PROTOCOL_HEAD
#define DEF_PROTOCOL_HEAD

#include <QByteArray>
#include <QDataStream>

namespace proto {
	enum PacketType {
		PckRequestSalt=0,
		PckSendSalt=1,
		PckRequestLogin=2,
		PckLoginRejected=3,
		PckLoginAccepted=4,

		PckDataAvailable=10,
		
		PckUndefined=-1
	};

	class DataPacket : public QByteArray {
		public:
			DataPacket(quint16 type) : type(type), stream(this, QIODevice::WriteOnly)
			{
				stream << type;
			}
			DataPacket(const DataPacket& p) :
				QByteArray(p), type(p.getType()), stream(this, QIODevice::WriteOnly) {}

			quint16 getType() const { return type; }

			void add_sized(const QByteArray& content) {
				reserve(size() + 2 + content.size());
				stream << (quint16)content.size();
				this->append(content);
			}
	
			static DataPacket make(const quint16 type, const QByteArray& content) {
				DataPacket out(type);
				out.add_sized(content);
				return out;
			}
			static DataPacket make(const quint16 type, const QString& content) {
				DataPacket out(type);
				out.add_sized(content.toUtf8());
				return out;
			}

			static bool getByteArray(QByteArray& out, QDataStream& in)
			{
				quint16 size;
				in >> size;

				char* buf = new char[size];
				if(in.readRawData(buf, size) != size)
				{
					delete[] buf;
					return false;
				}
				out.append(buf,size);
				delete[] buf;
				return true;
			}

		protected:
			quint16 type;
			QDataStream stream;
	};


	const QCryptographicHash::Algorithm HASH_ALGORITHM = QCryptographicHash::Sha3_512;
}

#endif//DEF_PROTOCOL_HEAD

