/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */


#include "FileAnalyzer.h"

FileAnalyzer::FileAnalyzer(QObject* parent) : QObject(parent), path(""), timer(this)
{
	timer.setInterval(3*1000);
	connect(&timer, SIGNAL(timeout()), this, SLOT(checkChanges()));
}

void FileAnalyzer::exec()
{
	timer.start();
}

void FileAnalyzer::checkChanges()
{
	QFile file(path);
	if(!file.exists())
		return;
	if(!file.open(QIODevice::ReadOnly))
		return;

	while(!file.atEnd())
	{
		QByteArray ba = file.readLine();
		if(ba.isEmpty())
			continue;

		QString line(ba);
		if(line.contains(QRegExp("^\\W*$"))) // only non-word
			continue;

		if(line.endsWith('\n'))
			line.truncate(line.size()-1);
		emit(lineAvailable(line));
	}

	file.remove();
}

