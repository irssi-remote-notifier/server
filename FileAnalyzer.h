/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */


#ifndef DEF_FILEANALYZER
#define DEF_FILEANALYZER

#include <QFile>
#include <QString>
#include <QTimer>
#include <QByteArray>
#include <QRegExp>

class FileAnalyzer : public QObject
{
	Q_OBJECT
	public:
		FileAnalyzer(QObject* parent=0);
		void exec();

		void setFile(QString _path) { path=_path; }
		void setTimerInterval(int interval) {
			if(interval > 1)
				timer.setInterval(interval);
		}

	signals:
		void lineAvailable(QString line);

	private slots:
		void checkChanges();

	private:
		QString path;
		QTimer timer;
};

#endif//DEF_FILEANALYZER

