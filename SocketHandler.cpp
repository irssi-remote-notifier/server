
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#include "SocketHandler.h"

SocketHandler::SslFiles SocketHandler::sslFiles;

SocketHandler::SocketHandler(QSslSocket* sock, QString* loginPassword, QObject* parent) :
	QObject(parent), sock(sock), addr(), port(), dummy(false), logged(false),
	loginPassword(loginPassword), expectedPass()
{
	startEncryption();
}
SocketHandler::SocketHandler(QHostAddress addr, quint16 port, QObject* parent) :
	QObject(parent), sock(), addr(addr), port(port), dummy(true), logged(false)
{
}

SocketHandler::~SocketHandler()
{
	if(!dummy) {
		delete sock;
	}
}

bool SocketHandler::sendData(const QByteArray& data)
{
	if(sock->write(proto::DataPacket::make(proto::PckDataAvailable, data)) > 0)
		return true;
	return false;
}
bool SocketHandler::sendData(const QString& data)
{
	return sendData(data.toUtf8());
}

void SocketHandler::startEncryption()
{
	sock->setPrivateKey(sslFiles.privkey);
	sock->setLocalCertificate(sslFiles.cert);

	connect(sock, SIGNAL(encrypted()), this, SLOT(sockEncrypted()));
	connect(sock, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(sockError(QAbstractSocket::SocketError)));
	connect(sock, SIGNAL(sslErrors(const QList<QSslError>&)),
			this, SLOT(sockSslError(const QList<QSslError>&)));
	connect(sock, SIGNAL(disconnected()), this, SLOT(sockDisconnected()));
	connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
	sock->startServerEncryption();
}

void SocketHandler::genSalt(QByteArray& out, size_t size) const
{
	out.reserve(out.size() + size);
	for(size_t curByte=0; curByte < size; curByte++)
	{
		char byte = (char)rand();
		out.append(byte);
	}
}

bool SocketHandler::checkPass(const QByteArray& pass) const
{
	if(expectedPass.size() != pass.size())
		return false;
	for(int pos=0; pos < expectedPass.size(); pos++)
	{
		if(expectedPass[pos] != pass[pos])
			return false;
	}
	return true;
}

void SocketHandler::beginAuth()
{
	QByteArray salt;
	genSalt(salt, 64);

	QCryptographicHash hasher(proto::HASH_ALGORITHM);
	hasher.addData(loginPassword->toUtf8());
	hasher.addData(salt);
	expectedPass = hasher.result();

	sendPacket(proto::DataPacket::make(proto::PckSendSalt, salt));
}

void SocketHandler::loginWith(const QByteArray& pass)
{
	if(expectedPass.isEmpty()) // No login request was sent
	{
		logged=false;
		sendPacket(proto::DataPacket(proto::PckLoginRejected));
		return;
	}
	else if(checkPass(pass))
	{
		logged = true;
		qDebug() << "Client logged in.";
		sendPacket(proto::DataPacket(proto::PckLoginAccepted));
	}
	else
	{
		logged=false;
		sendPacket(proto::DataPacket(proto::PckLoginRejected));
		expectedPass.clear();
		return;
	}
}

void SocketHandler::sockEncrypted()
{
	qDebug() << "Reached encrypted state!";
}

void SocketHandler::sockError(QAbstractSocket::SocketError)
{
	qDebug() << "Socket error : " << sock->errorString();
}
void SocketHandler::sockSslError(const QList<QSslError>& errors)
{
	for(int pos=0; pos < errors.size(); pos++)
		qDebug() << "Socket SSL error : " << errors[pos].errorString();
}

void SocketHandler::sockDisconnected()
{
	this->deleteLater();
}

void SocketHandler::readyRead()
{
	while(sock->bytesAvailable() > 0)
	{
		if(isLogged())
			sock->readAll(); // Discard data

		QDataStream stream(sock);

		quint16 packetType;
		stream >> packetType;

		QByteArray receivedBa;
		switch(packetType)
		{
			case proto::PckRequestSalt:
				beginAuth();
				break;
			case proto::PckRequestLogin:
				proto::DataPacket::getByteArray(receivedBa, stream);
				loginWith(receivedBa);
				break;
			default:
				sock->readAll(); // Unknown packet type, purge input.
		}
	}
}

void SocketHandler::sendPacket(const proto::DataPacket& pck)
{
	sock->write(pck);
}

