/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */


#include "ServWorker.h"

ServWorker::ServWorker(QObject* parent) :
	QObject(parent), settings("irssi_remote_notifier", "server"),
	fileAnalyzer(this)
{
	// Settings
	makeDefaultSettings();

	password=settings.value("password","").toString();
	if(password.isEmpty())
	{
		throw FatalException("No password set! Set it in your configuration file, or using "
				"the --settings argument.");
	}

	// File analyzer
	QString inputFilePath=settings.value("input_file","").toString();
	if(inputFilePath.isEmpty())
	{
		throw FatalException("No input file set! The server would never send anything. Closing. "
				"Set it in your configuration file, or using the --settings argument.");
	}
	fileAnalyzer.setFile(inputFilePath);

	// Output server
	SocketHandler::SslFiles sslFiles;
	sslFiles.privkey	= settings.value("ssl/privkey_path","").toString();
	sslFiles.cert		= settings.value("ssl/cert_path","").toString();
	if(sslFiles.privkey.isEmpty() || sslFiles.cert.isEmpty())
	{
		throw FatalException("Your SSL cetificate and key are not correctly set! Set it in your "
				"configuration file, or using the --settings argument.");
	}
	else if(!QFile::exists(sslFiles.privkey) || !QFile::exists(sslFiles.cert))
	{
		throw FatalException("Your SSL cetificate and key files does not exists! Set it in your "
				"configuration file, or using the --settings argument.");
	}

	SocketHandler::setSslFiles(sslFiles);

	server=new TcpServer(&password, this);

	// Launching file analyzer after server is up
	connect(&fileAnalyzer, SIGNAL(lineAvailable(QString)), server, SLOT(broadcast(QString)));
	fileAnalyzer.exec();
}

ServWorker::~ServWorker()
{
	delete server;
}

void ServWorker::exec()
{
	quint16 port = settings.value("port", 16667).toUInt();

	if(!server->listen(port))
		throw FatalException(server->errorString().toStdString().c_str());
}

void ServWorker::makeDefaultSettings()
{
	settings.setValue("password", settings.value("password","").toString());
	settings.setValue("port", settings.value("port",16667).toUInt());
	settings.setValue("input_file", settings.value("input_file","").toString());
	
	settings.setValue("ssl/privkey_path", settings.value("ssl/privkey_path","").toString());
	settings.setValue("ssl/cert_path", settings.value("ssl/cert_path","").toString());
}

