
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#include <QCoreApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>

#include <exception>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>

#include "ServWorker.h"
#include "exceptions.h"

int main(int argc, char** argv)
{
	if(argc >= 2 && strcmp(argv[1], "--settings") == 0) // settings mode
	{
		std::cerr << "To implement ; you can edit directly "
			<< "~/.config/irssi_remote_notifier/server.conf" << std::endl;
		return 0;
	}
	try {
		srand(time(NULL)+42); // Maybe change this one.
		QCoreApplication a(argc, argv);

		// Setup translator
		QString locale = QLocale::system().name().section('_', 0, 0);
		QTranslator translator;
		translator.load(QString("qt_") + locale,
				QLibraryInfo::location(QLibraryInfo::TranslationsPath));
		a.installTranslator(&translator);

		ServWorker worker;
		worker.exec();

		return a.exec();

	} catch(const FatalException& e) {
		std::cerr << "[FATAL] Error received: " << e.what() << std::endl;
	} catch(const std::exception& e) {
		std::cerr << "[FATAL] Standard exception catched: " << e.what() << std::endl;
	} catch(...) {
		std::cerr << "[FATAL] Unknown exception catched." << std::endl;
	}

	return EXIT_FAILURE;
}

