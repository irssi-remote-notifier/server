
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#include "TcpServer.h"

TcpServer::TcpServer(QString* password, QObject* parent) : QTcpServer(parent), password(password)
{}

bool TcpServer::listen(quint16 port)
{
	if(!QTcpServer::listen(QHostAddress::Any, port))
		return false;

	return true;
}

void TcpServer::incomingConnection(qintptr socketDescriptor)
{
	QSslSocket* incSocket = new QSslSocket();
	if(incSocket->setSocketDescriptor(socketDescriptor))
	{
		SocketHandler* handler = new SocketHandler(incSocket, password, this);
		socketHandlers.insert(handler);
		connect(handler, SIGNAL(destroyed(QObject*)), this, SLOT(sockDeleted(QObject*)));
	}
	else
		delete incSocket;
}

void TcpServer::broadcast(const QByteArray& data)
{
	for(QSet<SocketHandler*>::iterator curSocket=socketHandlers.begin();
			curSocket != socketHandlers.end(); ++curSocket)
	{
		if((*curSocket)->canSendData())
			(*curSocket)->sendData(data);
	}
}
void TcpServer::broadcast(const QString& data)
{
	broadcast(data.toUtf8());
}

void TcpServer::sockDeleted(QObject* obj)
{
	qDebug() << "Socket deleted.";
	SocketHandler* handler = qobject_cast<SocketHandler*>(obj);
	if(handler == 0)
		return;
	socketHandlers.remove(handler);
}

