
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#ifndef DEF_SOCKETHANDLER
#define DEF_SOCKETHANDLER

#include <QSslSocket>
#include <QList>
#include <QHostAddress>
#include <QByteArray>
#include <QDataStream>
#include <QCryptographicHash>

#include "protocol.h"

class SocketHandler : public QObject
{
	Q_OBJECT
	public:
		struct SslFiles {
			QString privkey, cert;
		};

		SocketHandler(QSslSocket* sock, QString* loginPassword, QObject* parent=0);
		SocketHandler(QHostAddress addr, quint16 port, QObject* parent=0); // Comparator only
		~SocketHandler();

		bool sendData(const QByteArray& data);
		bool sendData(const QString& data);

		QHostAddress getHostAddress() const { 
			if(!dummy) return sock->peerAddress();
			return addr;
		}
		quint16 getServerPort() const {
			if(!dummy) return sock->peerPort();
			return port;
		}

		bool isLogged() const {
			return logged;
		}
		bool isDummy() const {
			return dummy;
		}
		bool canSendData() const {
			return (!dummy) && logged && sock->isEncrypted();
		}

		bool operator<(const SocketHandler& sh) const { // Enables to use a set, increased speed
			int cmp = getHostAddress().toString().compare(
					sh.getHostAddress().toString());
			if(cmp == 0)
				return getServerPort() < sh.getServerPort();
			return (cmp < 0);
		}

		static void setSslFiles(const SslFiles& files) { SocketHandler::sslFiles = files; }
	
	private: //meth
		void startEncryption();
		void genSalt(QByteArray& out, size_t size) const;
		bool checkPass(const QByteArray& pass) const; // SECURITY CRITICAL
		void beginAuth(); // Generates, stores and sends salt. Requests password.
		void loginWith(const QByteArray& pass);
	
	private slots:
		void sockEncrypted();
		void sockError(QAbstractSocket::SocketError error);
		void sockSslError(const QList<QSslError>& errors);
		void sockDisconnected();
		void readyRead();

		void sendPacket(const proto::DataPacket& pck);

	private:
		QSslSocket* sock;
		QHostAddress addr;
		quint16 port;
		bool dummy;

		bool logged;

		QString* loginPassword;
		QByteArray expectedPass;
		static SslFiles sslFiles;
};

#endif//DEF_SOCKETHANDLER

